# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::UtilTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Util' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  return 'Incomplete module';
  use_ok $test->class;
}

sub idn : Tests(5) {
  use_ok shift->class;

  my $domain = 'ö.example';

  my $ascii = Amavis::Util::idn_to_ascii($domain);
  is($ascii, 'xn--nda.example', 'idn_to_ascii');

  my $utf8 = Amavis::Util::idn_to_utf8($ascii);
  is($utf8, $domain, 'idn_to_utf8');

  my $address = 'local@ö.example';

  $ascii = Amavis::Util::mail_addr_idn_to_ascii($address);
  is($ascii, 'local@xn--nda.example', 'mail_addr_idn_to_ascii');

  $utf8 = Amavis::Util::mail_addr_decode($ascii, 1);
  is($utf8, $address, 'mail_addr_decode');
}

1;
